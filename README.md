# gibmount

WoW addon adding better interface for viewing your mounts and companions. Besides cleaner interface you can also favorite your pets and then summon random one of them with a handy macro. Summoning mounts is coded in a smart way so that the pet is chosen from the best possible set (flight/run speed skill, swimming etc).
It's still work in progress so there are bugs, one of them is the need to reload your ui after learning new mount/transportation skill (maybe it's not necessarily as much of a bug as it is just me forgetting to implement the event hook).
