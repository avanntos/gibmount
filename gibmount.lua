function gibmount_OnLoad()
    SLASH_gibmount1= "/gib";
    SlashCmdList["gibmount"] = gib;
    GibMount = {};
    GibMount.favTexture = ""
    GibMount.chosenType = "MOUNT"
    GibMount.searchPhrase = ""
    GibMount.macroDetailedDB = {}
    GibMount.macroDetailedDB.lastSummoned = nil
    GibMount.companionDB = {}
    GibMount.matchedCompanionDB = {}
    GibMount.companionsInitialized = {};
    GibMount.texturesInitialized = false;
    GibMount.initialized = false
    favTexture = nil
    GibMountScrollBar:Show();
    gibmount_Frame:RegisterForDrag("LeftButton");
    GibMount.DEBUG = false
    tinsert(UISpecialFrames, gibmount_Frame:GetName());



    gameoptionsframe = CreateFrame("Button","gibmountgameoptions", GameMenuFrame, "UIPanelButtonTemplate")
    gameoptionsframe:SetHeight(20)
    gameoptionsframe:SetWidth(145)
    gameoptionsframe:SetText("gibmount")
    gameoptionsframe:ClearAllPoints()
    gameoptionsframe:SetPoint("TOP", 0, 30)
    gameoptionsframe:RegisterForClicks("AnyUp")
    gameoptionsframe:SetScript("OnClick", function()
        GameMenuFrame:Hide()
        gib()
    end )



    this:RegisterEvent("ADDON_LOADED");
    this:RegisterEvent("VARIABLES_LOADED")
    print("GibMount loaded.")
end

function gib()
--     gibmount_searchBox:Hide();
    gibmount_Frame:Show();
end

function initCompanionDB(gibType)
    GibDebugPrint("querrying "..gibType)
    GibMount.companionDB[gibType] = {}
    GibMount.matchedCompanionDB[gibType] = {}
    GibMount.companionDB[gibType].count = GetNumCompanions(gibType)

    GibMount.mountsDB = {};
    local favoritesCount = 0
    for i=1,GibMount.companionDB[gibType].count do

        local _,lName,lID,lIcon = GetCompanionInfo(gibType, i)
        local fav = 0
--         print(lIcon)
        if(GibMountData.favorites[gibType][lID]) then
            fav = true
            favoritesCount = favoritesCount + 1
        else
            fav = false
        end

        GibMount.companionDB[gibType][i] = {companionName = lName, companionIcon = lIcon, companionID = lID, listID = i, selected=false, favorited=fav, mountType=-1}
        if gibType == "MOUNT" then
            GibMount.companionDB[gibType][i].mountType = determineMountType(GibMount.companionDB[gibType][i])
--             print(GibMount.companionDB[gibType][i].mountType)
        end
    end

    GibMount.companionDB[gibType].favCount = favoritesCount
    sortDB(GibMount.companionDB[gibType])

    matchCompanions(gibType)
    initMacroDB(gibType)
    GibMount.companionsInitialized[gibType] = true


end

function addToTable(DBtable, item)
    DBtable.count = DBtable.count + 1
    DBtable[DBtable.count] = item
end

function parseTooltipContent(mountSpellID)

    GibMountScanningTooltip:ClearLines()
    GibMountScanningTooltip:SetOwner(WorldFrame, "ANCHOR_NONE")
    GibMountScanningTooltip:SetHyperlink("spell:"..mountSpellID)

--     GibDebugPrint("Description nr lines: "..GibMountScanningTooltip:NumLines())

    local descriptionText = nil
    local region = select(14, GibMountScanningTooltip:GetRegions())
    if region and region:GetObjectType() == "FontString" then
        descriptionText = region:GetText() -- string or nil
    end
--     if descriptionText ~= nil then
--         GibDebugPrint("Description contents:["..descriptionText.."]")
--     end
    GibMountScanningTooltip:Hide()

    local fastMount = descriptionText:match("very fast") or descriptionText:match("extremely fast")
    local flyingMount = descriptionText:match("Outland") or descriptionText:match("Northrend")
    local mType = nil
    if flyingMount and fastMount then
        mType = 4
    elseif flyingMount then
        mType = 3
    elseif fastMount then
        mType = 2
    else
        mType = 1
    end

    return mType
--     GibDebugPrint("Mount type: "..mType)
end

function determineMountType(mount)
    -- types are as follows:    0 - swimming
    --                          1 - slow ground
    --                          2 - fast ground
    --                          3 - slow flight
    --                          4 - fast flight
    --                          10 - adaptive (ground)
    --                          11 - adaptive (flying + ground)
    --                          12 - adaptive (flying)
    --                          20 - aq
    --                          -1 - unknown

    local overrides = { [64731] = 0,            -- sea turtle
                        [30174] = 1,            -- tcg turtle
                        [579]   = 2,            -- Red Wolf
                        [58983] = 10,           -- big blizzard bear
                        [48025] = 11,           -- headless horseman
                        [71342] = 11,           -- valentines rocket
                        [75614] = 11,           -- celestial steed
                        [54729] = 12,           -- dk flying class mount
                        [75973] = 12,           -- X-53 touring rocket
                        [25953] = 20,           -- Blue     AQ tank
                        [26056] = 20,           -- Green    AQ tank
                        [26054] = 20,           -- Red      AQ tank
                        [26055] = 20    }       -- Yellow   AQ tank

    return overrides[mount.companionID] or parseTooltipContent(mount.companionID)
end

function initMacroDB(gibType)
    if(gibType == "MOUNT") then
        GibMount.macroDetailedDB["MOUNT"] = {   ["slow_ground"] =  {["count"] = 0, ["favCount"] = 0},
                                                ["ground"]      =  {["count"] = 0, ["favCount"] = 0},
                                                ["slow_flying"] =  {["count"] = 0, ["favCount"] = 0},
                                                ["flying"]      =  {["count"] = 0, ["favCount"] = 0},
                                                ["swimming"]    =  {["count"] = 0, ["favCount"] = 0},
                                                ["aq"]          =  {["count"] = 0, ["favCount"] = 0}   }

        local lookup = {    [0]     = {[1] = "swimming"},
                            [1]     = {[1] = "slow_ground"},
                            [2]     = {[1] = "ground"},
                            [3]     = {[1] = "slow_flying"},
                            [4]     = {[1] = "flying"},
                            [10]    = {[1] = "slow_ground", [2] = "ground"},
                            [11]    = {[1] = "slow_ground", [2] = "ground", [3] = "slow_flying", [4] = "flying"},
                            [12]    = {[1] = "slow_flying", [2] = "flying"},
                            [20]    = {[1] = "aq"},
                            [-1]    = {[1] = "unknown"}}

        local DB = GibMount.macroDetailedDB["MOUNT"]

        for i=1,GibMount.companionDB["MOUNT"].count do
            -- TO CHANGE
            local mType = GibMount.companionDB["MOUNT"][i].mountType
            -- ---------
            local mFav = GibMount.companionDB["MOUNT"][i].favorited
            for _,mmType in ipairs(lookup[mType]) do
                addToTable(DB[mmType], GibMount.companionDB["MOUNT"][i].listID)
                if mFav then
                    DB[mmType].favCount = DB[mmType].favCount + 1
                end
            end

        end
    end
end

function initTextures()
--         GibDebugPrint("tex init")
    GibMount.iconTextures = {};
    GibMount.starTextures = {};
    GibMount.iconTextures.selected = {}
    GibMount.iconTextures.selected[GibMount.chosenType] = nil

    GibMount.iconDecorationTextures = {}

    for record=1,12 do
--             getglobal("gibmount_spellIcon"..line):RegisterForDrag("LeftButton")
        GibMount.iconTextures[record] = getglobal("gibmount_entryContainer"..record.."_icon"):CreateTexture()
        GibMount.iconTextures[record]:SetPoint("CENTER")
        GibMount.iconTextures[record]:SetSize(40, 40)

        GibMount.starTextures[record] = getglobal("gibmount_entryContainer"..record.."_iconFrame_star"):CreateTexture()
        getglobal("gibmount_entryContainer"..record.."_iconFrame_star"):SetHighlightTexture("Interface\\Addons\\gibmount\\res\\fav_star_empty")
        GibMount.starTextures[record]:SetPoint("CENTER")
        GibMount.starTextures[record]:SetSize(16, 16)
        GibMount.starTextures[record]:SetTexture("");
        GibMount.starTextures[record].set=false;

        GibMount.iconDecorationTextures[record] = getglobal("gibmount_entryContainer"..record.."_iconFrame_tx")
        GibMount.iconDecorationTextures[record]:SetTexture("Interface\\Addons\\gibmount\\res\\icon_decor")

    end

    GibMount.texturesInitialized = true
end

function GibMountInit()
    GibDebugPrint("INIT")
    GibMount.chosenType = "CRITTER"
    initCompanionDB("CRITTER");
    GibMount.chosenType = "MOUNT"
    initCompanionDB("MOUNT");

    initTextures()

    GibMount.favTexture = gibNewIcon("CRITTER")
    GibDebugPrint("tex:"..GibMount.favTexture)
    circleActionIcon:SetTexture(GibMount.favTexture)
    actionIcon_button:SetScript("OnDragStart",  function (self, button)
                                                    gibTheMacro()
                                                end);
    GibDebugPrint("~INIT")
    GibMount.initialized = true


end

function GibMountScrollBar_Update()
--     if not GibMount.companionsInitialized[GibMount.chosenType] then
--     end
--
--     if not GibMount.texturesInitialized then
--         initTextures()
--     end

    if(not GibMount.initialized) then
        GibMountInit()
    end

    if(GibMount.favTexture ~= nil) then
        SetPortraitToTexture(circleActionIcon, GibMount.favTexture)
    end
--     local line;
--     GibDebugPrint("scroll update")
    local record;
    local lineplusoffset;
    local compDB = GibMount.matchedCompanionDB
--     local compDB = GibMount.companionDB
--     FauxScrollFrame_Update(GibMountScrollBar, math.ceil(GibMount.companionDB[GibMount.chosenType].count/2), 12, 50);
    FauxScrollFrame_Update(GibMountScrollBar, math.ceil(compDB[GibMount.chosenType].count/12)*3, 3, 60);
    selectEntry();
    for record=1,12 do
        local entryContainer =  getglobal("gibmount_entryContainer"..record);
        local nameEntry =       getglobal("gibmount_entryContainer"..record.."_text");
        local nameEntryText =   getglobal("gibmount_entryContainer"..record.."_text_fontString");
        local iconEntry =       getglobal("gibmount_entryContainer"..record.."_icon");
        local iconFrameStar =   getglobal("gibmount_entryContainer"..record.."_iconFrame_star");
        entryContainer:Show();

        local offset = 4*FauxScrollFrame_GetOffset(GibMountScrollBar)
        local lineplusoffset = record + offset
--         GibDebugPrint("l+o:"..lineplusoffset)
--         GibDebugPrint(lineplusoffset.."?="..GibMount.companionDB[GibMount.chosenType].count)
        if lineplusoffset <= compDB[GibMount.chosenType].count then
--             GibDebugPrint("debug")
            local companion = compDB[GibMount.chosenType][lineplusoffset]

            GibMount.iconTextures[record]:SetTexture(companion.companionIcon)

            if(GibMount.companionDB[GibMount.chosenType].selected == companion.listID) then
                selectEntry(record);
            end

            if(companion.favorited) then
                if(not GibMount.starTextures[record].set) then
                    toggleEntryFavorite(GibMount.starTextures[record])
                end
            else
                if(GibMount.starTextures[record].set) then
                    toggleEntryFavorite(GibMount.starTextures[record])
                end
            end
            local tmpline = lineplusoffset
            nameEntryText:SetText(companion.companionName);
            if GibMount.chosenType == "MOUNT" then
                local r,g,b = mountTypeToColor(companion.mountType)
                nameEntryText:SetTextColor(r, g, b, 1);
            end
            nameEntry:SetScript("OnClick",      function (self, button)
                                                    GibMount.companionDB[GibMount.chosenType].selected = companion.listID;
                                                    selectEntry(record);
                                                    GibDebugPrint(companion.mountType)
--                                                     GibDebugPrint(companion.companionIcon)
                                                end);

            iconFrameStar:SetScript("OnClick",  function (self, button)
                                                    toggleEntryFavorite(GibMount.starTextures[record], companion);

                                                    if(companion.favorited) then
                                                        GibMount.companionDB[GibMount.chosenType].favCount = GibMount.companionDB[GibMount.chosenType].favCount - 1
                                                    else
                                                        GibMount.companionDB[GibMount.chosenType].favCount = GibMount.companionDB[GibMount.chosenType].favCount + 1
                                                    end

                                                    companion.favorited = not companion.favorited

                                                    sortDB(GibMount.companionDB[GibMount.chosenType])
                                                    matchCompanions(GibMount.chosenType)
                                                    GibMountData.favorites[GibMount.chosenType][companion.companionID] = not GibMountData.favorites[GibMount.chosenType][companion.companionID]
                                                    initMacroDB(GibMount.chosenType)
                                                    GibMountScrollBar_Update()
                                                end);
            iconEntry:SetScript("OnDragStart",  function (self, button)
                                                    ClearCursor();
                                                    PickupCompanion(GibMount.chosenType, companion.listID);
                                                end);
            iconEntry:SetScript("OnDragStop",   function (self, button)
                                                    GibMount.iconTextures[record]:SetAlpha(1)
                                                end);
            iconEntry:SetScript("OnClick",      function (self, button)
                                                    CallCompanion(GibMount.chosenType, companion.listID);
                                                end);
            iconEntry:SetScript("OnMouseDown",  function (self, button)
                                                    GibMount.iconTextures[record]:SetAlpha(0.45)
                                                end);
            iconEntry:SetScript("OnMouseUp",    function (self, button)
                                                     GibMount.iconTextures[record]:SetAlpha(1)
                                                end);

        else
            entryContainer:Hide();
        end
    end
end

function swapCompanionTypes()
    GibDebugPrint("swapping")
    selectEntry()

    GibMount.favTexture = gibNewIcon(GibMount.chosenType)
--     GibDebugPrint("tex:"..GibMount.favTexture)
    circleActionIcon:SetTexture(GibMount.favTexture)

    if(GibMount.chosenType == "MOUNT") then
        GibMount.chosenType = "CRITTER"
    else
        GibMount.chosenType = "MOUNT"
    end
    gibmount_searchBox:SetText("");
--     GibMount.companionsInitialized[] = false
    if(GibMount.companionDB[GibMount.chosenType] == nil) then
        initCompanionDB(GibMount.chosenType)
    end

    actionIcon_button:SetScript("OnDragStart",  function (self, button)
                                                    gibTheMacro()
                                                end);

    GibMountScrollBar_Update()
end

function gibmount_searchBoxEscapePressed(textbox)
    local text = textbox:GetText();
    if(text == "") then
        textbox:ClearFocus()
    else
        textbox:SetText("");
    end
--     textbox:Hide();
--     EditBox:SetText("text") - Set the text contained in the edit box

end

function matchCompanions(compType)
    GibDebugPrint("match "..compType)
    GibMount.matchedCompanionDB[compType].count = 0
    local i = 1
    local favs = 0
    for i=1,GibMount.companionDB[compType].count do
        local foundString = string.find(string.lower(GibMount.companionDB[compType][i].companionName), GibMount.searchPhrase)
        if(foundString ~= nil) then
            if(GibMount.companionDB[compType][i].favorited) then
                favs = favs + 1
            end
            GibMount.matchedCompanionDB[compType].count = GibMount.matchedCompanionDB[compType].count + 1
            GibMount.matchedCompanionDB[compType][GibMount.matchedCompanionDB[compType].count] = GibMount.companionDB[compType][i]
        end

    end
    GibMount.matchedCompanionDB[compType].favCount = favs
--     GibDebugPrint("c="..GibMount.matchedCompanionDB[compType].count)
--     for i=1,GibMount.matchedCompanionDB[compType].count do
--         GibDebugPrint(GibMount.matchedCompanionDB[compType][i].companionName)
--     end
end

function gibmount_searchBoxTextChanged(textbox)
--     crash on '['
    GibMount.searchPhrase = string.lower(textbox:GetText());
    matchCompanions(GibMount.chosenType);
    GibMountScrollBar_Update();
end

function clearEntry(containerID)
    getglobal("gibmount_entryContainer"..containerID.."_iconFrame"):Show()
    getglobal("gibmount_entryContainer"..containerID.."_iconFrame_select"):Hide()
end

function selectEntry(containerID)
--     entries are the same: nothing happens
    local prevContainerID = GibMount.iconTextures.selected[GibMount.chosenType]
    if(containerID ~= prevContainerID) then
--      clean currently chosen entry unless it's 'nil'
        if(prevContainerID ~= nil) then
            clearEntry(prevContainerID);
        end
--      chose container if it's not 'nil'
        if(containerID ~= nil) then
            GibMount.iconTextures.selected[GibMount.chosenType] = containerID;
            getglobal("gibmount_entryContainer"..containerID.."_iconFrame"):Hide()
            getglobal("gibmount_entryContainer"..containerID.."_iconFrame_select"):Show()
        else
--          unmark marked container
            GibMount.iconTextures.selected[GibMount.chosenType] = nil
            clearEntry(prevContainerID)
        end
        return
    end
--     GibDebugPrint("omitted")
end

function toggleEntryFavorite(texture)
    if(texture.set) then
        texture.set = false
        texture:SetTexture("");
    else
        texture.set = true
        texture:SetTexture("Interface\\Addons\\gibmount\\res\\fav_star");
    end

end
-- is a < b
function gibmount_sort(a, b)
    if(a==nil or b==nil) then return false end
--     GibDebugPrint("comparing "..a.companionName.." and "..b.companionName)
    if( (a.favorited and b.favorited) or ((not a.favorited) and (not b.favorited))) then
        return a.listID < b.listID
    end
--     GibDebugPrint(a.favorited)
    if(a.favorited) then
        return true
    end
    return false
end

function sortDB(DB)
    GibDebugPrint("sort")
    local newDB = {}
    for i=1,DB.count do
        newDB[i] = DB[i]
    end
    table.sort(newDB,gibmount_sort)
    newDB.count = DB.count
    newDB.favCount = DB.favCount
    if(DB.selected ~= nil) then
        newDB.selected = DB.selected
    end
    GibMount.companionDB[GibMount.chosenType] = newDB
end

function savedVariablesHandler()
    if(GibMountData == nil) then
--         GibDebugPrint("creating empty db")
        GibMountData = {}
        GibMountData.favorites = {}
        GibMountData.favorites["MOUNT"] = {}
        GibMountData.favorites["CRITTER"] = {}
    end
end

function gibmount_frameEvents(event, arg1, arg2)
--     GibDebugPrint("event fired "..arg1.." "..arg2)
    if(arg1 == "ADDON_LOADED" and arg2 == "gibmount") then
        savedVariablesHandler()
    end
end

--  bug with index persisting in different mount types
function rangeGibRandomFav(numOne, numTwo, lastRolled)
    local ceiling = numTwo
    if numOne > 0 then
       ceiling = numOne
    end
    GibDebugPrint("ceiling"..ceiling)
    local index = -1
    repeat
        index = math.random(ceiling)
        GibDebugPrint("randomizing")
    until index ~= lastRolled or ceiling == 1

    return index

    --[[if numOne > 0 then
        return math.random(numOne)
    else
        return math.random(numTwo)
    end]]
end

function gibRandomFav(gibType)
    local topRand = 1
    local countt = GibMount.companionDB[gibType].favCount
--     GibDebugPrint("countt"..countt)
    if(countt > 0) then
        return math.random(countt)
    end
    countt = GibMount.companionDB[gibType].count
    if countt > 0 then
        return math.random(countt)
    else
        return -1
    end

end

function gibNewIcon(gibType)
--     GibDebugPrint("gib gib")
    local index = gibRandomFav(gibType)
    if index > 0 then
        return  GibMount.companionDB[gibType][index].companionIcon
    end

    GibDebugPrint("fallthrough")
    if(gibType == "MOUNT") then
        return "Interface\\Icons\\Ability_Mount_PolarBear_White"
    else
        return "Interface\\Icons\\Ability_Mount_FireRavenGodMount"
    end

end

function GibMountMacro(type)

-- swimming
-- fast flight
-- slow flight
-- fast ground
-- slow ground

    if(not GibMount.initialized) then
        GibMountInit()
    end
    if(type == 1) then  -- mount
--         GibDebugPrint("summon mount")
        if UnitCastingInfo("player") then
            GibDebugPrint("casting")
            return
        end

        if IsMounted() and not IsFlying() then
            return Dismount()
        end
        local mountDB = GibMount.macroDetailedDB["MOUNT"]
        local mountType = ""
        if IsSwimming() and mountDB["swimming"].count > 0 then
            GibDebugPrint("summon swimming mount")
            GibMount.macroDetailedDB.lastSummoned = rangeGibRandomFav(mountDB["swimming"].favCount, mountDB["swimming"].count, GibMount.macroDetailedDB.lastSummoned)
            CallCompanion("MOUNT", mountDB["swimming"][GibMount.macroDetailedDB.lastSummoned])
            return
        end
        local currentZone = GetZoneText()
        local skills = GetNumSkillLines()
        local ridingSkill = 0
        for i=1,skills do
            local skillName,_,_,skillRank = GetSkillLineInfo(i)
            if skillName == "Riding" then
                ridingSkill = skillRank
            end
        end
--         GibDebugPrint("Riding skill: "..ridingSkill..".")
        -- shift override
        -- zone not flyable OR in wintergrasp during battle
        local notFlyable = not IsFlyableArea() or (currentZone == "Wintergrasp" and GetWintergraspWaitTime() == nil)
        if not IsFlyableArea() or ridingSkill<225 or (currentZone == "Wintergrasp" and GetWintergraspWaitTime() == nil) then
--             GibDebugPrint("ground mount time")
            if IsMounted() and IsShiftKeyDown() then
                GibDebugPrint("shift key down")
            end
            if ridingSkill >= 150 then
                GibDebugPrint("fast ground mount")
                if (currentZone == "Ruins of Ahn'Qiraj" or currentZone == "Ahn'Qiraj") and mountDB["aq"].count > 0 then
--                     GibDebugPrint("-> aq mounts")
                    GibMount.macroDetailedDB.lastSummoned = rangeGibRandomFav(mountDB["aq"].favCount, mountDB["aq"].count, GibMount.macroDetailedDB.lastSummoned)
                    CallCompanion("MOUNT", mountDB["aq"][GibMount.macroDetailedDB.lastSummoned])
                else
                    GibMount.macroDetailedDB.lastSummoned = rangeGibRandomFav(mountDB["ground"].favCount, mountDB["ground"].count, GibMount.macroDetailedDB.lastSummoned)
                    CallCompanion("MOUNT", mountDB["ground"][GibMount.macroDetailedDB.lastSummoned])
                end
            else
--                 GibDebugPrint("slow ground mount")
                GibMount.macroDetailedDB.lastSummoned = rangeGibRandomFav(mountDB["slow_ground"].favCount, mountDB["slow_ground"].count, GibMount.macroDetailedDB.lastSummoned)
                CallCompanion("MOUNT", mountDB["slow_ground"][GibMount.macroDetailedDB.lastSummoned])
            end

        else
            if ridingSkill >= 300 then
--                 GibDebugPrint("fast flying mount time")
                GibMount.macroDetailedDB.lastSummoned = rangeGibRandomFav(mountDB["flying"].favCount, mountDB["flying"].count, GibMount.macroDetailedDB.lastSummoned)
                CallCompanion("MOUNT", mountDB["flying"][GibMount.macroDetailedDB.lastSummoned])
            else
--                 GibDebugPrint("slow flying mount time")
                GibMount.macroDetailedDB.lastSummoned = rangeGibRandomFav(mountDB["slow_flying"].favCount, mountDB["slow_flying"].count, GibMount.macroDetailedDB.lastSummoned)
                CallCompanion("MOUNT", mountDB["slow_flying"][GibMount.macroDetailedDB.lastSummoned])
            end
        end
    else                -- pet
--         GibDebugPrint("summon pet")
        local petIndex = gibRandomFav("CRITTER")
        if petIndex > 0 then
            CallCompanion("CRITTER", GibMount.matchedCompanionDB["CRITTER"][petIndex].listID)
        end
    end
end

function gibTheMacro()

    local nMacro,_ = GetNumMacros()
    local myMacroName = ""

    if(GibMount.chosenType == "MOUNT") then
        myMacroName = "GibMountMacroMount"
    elseif(GibMount.chosenType == "CRITTER") then
        myMacroName = "GibMountMacroPet"
    else
        return
    end

    local macroFound = false
    local macroId = -1
    for i=1,nMacro do
        local mName, mIcon,_ = GetMacroInfo(i)

        if(mName == myMacroName) then
            macroId = i
            macroFound = true
        end

        GibDebugPrint(mName.." "..mIcon)
    end

    if(not macroFound) then
        local summonType = -1
        if(GibMount.chosenType == "MOUNT") then
            summonType = 1
        else
            summonType = 2
        end

        MacroFrame:Hide()
        macroId = CreateMacro(myMacroName, 34, "/run GibMountMacro("..summonType..");", nil);
        GibDebugPrint("macro created")
    end

    if(macroId ~= -1) then
        PickupMacro(macroId)
    end
end
    -- types are as follows:    0 - swimming
    --                          1 - slow ground
    --                          2 - fast ground
    --                          3 - slow flight
    --                          4 - fast flight
    --                          10 - adaptive (ground)
    --                          11 - adaptive (flying + ground)
    --                          12 - adaptive (flying)
    --                          20 - aq
    --                          -1 - unknown
function mountTypeToColor(type)
    local lookup = {    [0]     = {["r"] = "0",     ["g"] = "0.8",      ["b"] = "1"},       --  swimming
                        [1]     = {["r"] = "0",     ["g"] = "0.439",    ["b"] = "0.867"},   --  slow ground
                        [2]     = {["r"] = "0.639", ["g"] = "0.208",    ["b"] = "0.934"},   --  fast ground
                        [3]     = {["r"] = "0",     ["g"] = "0.439",    ["b"] = "0.867"},   --  slow flight
                        [4]     = {["r"] = "0.639", ["g"] = "0.208",    ["b"] = "0.934"},   --  fast flight
                        [10]    = {["r"] = "0.639", ["g"] = "0.208",    ["b"] = "0.934"},   --  adaptive ground
                        [11]    = {["r"] = "0.639", ["g"] = "0.208",    ["b"] = "0.934"},   --  adaptive ground + flying
                        [12]    = {["r"] = "0.639", ["g"] = "0.208",    ["b"] = "0.934"},   --  adaptive flying
                        [20]    = {["r"] = "1",     ["g"] = "0.502",    ["b"] = "0"},       --  aq
                        [-1]    = {["r"] = "0",     ["g"] = "0",        ["b"] = "0"}  }     --  unknown
    local lookup2 = {   [0]     = {["r"] = "0",     ["g"] = "0.8",      ["b"] = "1"},       --  swimming
                        [1]     = {["r"] = "0",     ["g"] = "0.439",    ["b"] = "0.867"},   --  slow ground
                        [2]     = {["r"] = "0.588", ["g"] = "0.286",    ["b"] = "0.69"},    --  fast ground
                        [3]     = {["r"] = "0",     ["g"] = "0.439",    ["b"] = "0.867"},   --  slow flight
                        [4]     = {["r"] = "0.588", ["g"] = "0.286",    ["b"] = "0.69"},    --  fast flight
                        [10]    = {["r"] = "0.588", ["g"] = "0.286",    ["b"] = "0.69"},    --  adaptive ground
                        [11]    = {["r"] = "0.588", ["g"] = "0.286",    ["b"] = "0.69"},    --  adaptive ground + flying
                        [12]    = {["r"] = "0.588", ["g"] = "0.286",    ["b"] = "0.69"},    --  adaptive flying
                        [20]    = {["r"] = "1",     ["g"] = "0.502",    ["b"] = "0"},       --  aq
                        [-1]    = {["r"] = "0",     ["g"] = "0",        ["b"] = "0"}  }     --  unknown
    local gibColor = lookup2[type]
--     return 0.247, 0.392, 0.753
--     return 0.588, 0.286, 0.69 -- darker
--     return 0.588, 0.392, 0.753
    return gibColor.r, gibColor.g, gibColor.b
end
-- local skills = GetNumSkillLines() for i=1,skills do local skillName,_,_,skillRank = GetSkillLineInfo(i) if skillName == "Riding" then print("Riding skill: "..skillRank..".") end end

function GibDebugPrint(text)
    if GibMount.DEBUG and text ~= nil then
        GibDebugPrint(text)
    end
end
